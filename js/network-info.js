let provider = ethers.getDefaultProvider('rinkeby');
let netStats = document.getElementById("netStats");

function getLatestBlock() {
    provider.getBlockNumber().then((blockNumber) => {
        console.log("Current block number: " + blockNumber);
        netStats.innerHTML = 'Block: #' + blockNumber
    });
}

window.setInterval(function () {
    getLatestBlock()
}, 15000);

getLatestBlock()